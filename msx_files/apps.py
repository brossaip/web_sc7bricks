from django.apps import AppConfig


class MsxFilesConfig(AppConfig):
    name = 'msx_files'
